/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bsq.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/15 13:43:21 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/20 22:42:49 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BSQ_H
# define BSQ_H

# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# define BUF_SIZE 1000

enum	e_type
{
	EMPT = 0,
	OBST = 1,
	FULL = 2
};

enum	e_std_or_file
{
	STDIN = 1,
	FILEN = 0
};

typedef struct	s_vector
{
	int			x;
	int			y;
}				t_vec;

typedef struct	s_map
{
	char		**map;
	int			width;
	int			height;
	char		symbols[3];
	int			x;
	int			y;
	int			l;
}				t_map;

typedef struct	s_fstat
{
	int			fd;
	long int	ch;
	int			lb;
	int			len;
}				t_fstat;

void			caller(char *filename, int stdin_filen);

int				get_fstat(t_fstat *fstat, char *filename, int stdin_filen);
int				get_file(t_fstat fstat, char **out, char *filename, int sf);
int				get_tab(t_map *map, char *filename, int stdin_filen);
int				get_max_len(char *str);
int				get_params(char *to_split, t_map *map);

int				check_map(t_map map);
int				check(t_map map);

char			**split_to_tab(char *str);
char			**fill_tab(char **tab, char *str, unsigned int word_count);
unsigned int	count_words(char *str);
char			*str_ncpy(char *dest, char *src, unsigned int n);
int				charset(char c);

int				str_len(char *str);
void			pstr(char *str);
void			perrstr(char *str);
void			pchar(char c);
void			print_map(t_map map);

void			find_biggest_square(t_map map, t_vec coord);
void			update(int size, t_vec max, t_map map);
int				check_empty(t_map map, t_vec *curr, t_vec stop, t_vec coord);

#endif
