/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/19 15:12:38 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/20 14:02:44 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

void	caller(char *filename, int stdin_filen)
{
	int		i;
	t_map	map;

	map = (t_map){0, 0, 0, {0, 0, 0}, 0, 0, 0};
	if ((get_tab(&map, filename, stdin_filen) == 0))
		return ;
	if (check_map(map) == 0)
	{
		perrstr("map error\n");
		return ;
	}
	find_biggest_square(map, (t_vec){0, 1});
	print_map(map);
	i = 0;
	while (i < map.height && map.map[i])
		free(map.map[i++]);
	free(map.map);
}

int		main(int argc, char *argv[])
{
	int i;

	if (argc == 1)
		caller("no_file", STDIN);
	else if (argc >= 2)
	{
		i = 1;
		while (i < argc)
		{
			caller(argv[i], FILEN);
			i++;
		}
	}
	return (0);
}
