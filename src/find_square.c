/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_square.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/20 15:33:58 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/20 22:43:20 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

int		check_empty(t_map map, t_vec *curr, t_vec stop, t_vec coord)
{
	while (curr->y < stop.y)
	{
		if (map.map[curr->y][curr->x] != map.symbols[EMPT])
			return (0);
		curr->y++;
	}
	while (curr->x >= coord.x)
	{
		if (map.map[curr->y][curr->x] != map.symbols[EMPT])
			return (0);
		curr->x--;
	}
	return (1);
}

int		find_square_size(t_map map, t_vec coord)
{
	t_vec	curr;
	t_vec	stop;
	int		size;

	curr = (t_vec){0, 0};
	stop = (t_vec){0, 0};
	stop.y = coord.y + 1;
	stop.x = coord.x + 1;
	size = 1;
	curr.y = coord.y;
	if (map.map[coord.y][coord.x] != map.symbols[EMPT])
		return (0);
	while (stop.x <= map.width && stop.y <= map.height)
	{
		curr.x = stop.x;
		curr.y = coord.y;
		if (check_empty(map, &curr, stop, coord) == 0)
			return (size);
		stop.y++;
		stop.x++;
		size++;
	}
	return (size);
}

void	update(int size, t_vec max, t_map map)
{
	t_vec pos;

	pos.y = max.y;
	while (pos.y < max.y + size)
	{
		pos.x = max.x;
		while (pos.x < max.x + size)
		{
			map.map[pos.y][pos.x] = map.symbols[FULL];
			pos.x++;
		}
		pos.y++;
	}
}

void	find_biggest_square(t_map map, t_vec coord)
{
	int		max_size;
	int		i;
	t_vec	max;

	max_size = 0;
	while (map.map[coord.y + max_size])
	{
		coord.x = 0;
		while (map.map[coord.y][coord.x + max_size])
		{
			i = find_square_size(map, coord);
			if (i > max_size)
			{
				max_size = i;
				max.x = coord.x;
				max.y = coord.y;
			}
			coord.x++;
		}
		coord.y++;
	}
	update(max_size, max, map);
}
