/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/19 12:28:22 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/20 14:03:08 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

void	pstr(char *str)
{
	write(1, str, str_len(str));
}

void	perrstr(char *str)
{
	write(2, str, str_len(str));
}

void	pchar(char c)
{
	write(1, &c, 1);
}

int		str_len(char *str)
{
	int	len;

	len = 0;
	while (str[len])
		len++;
	return (len);
}

void	print_map(t_map map)
{
	map.y = 1;
	while (map.y <= map.height)
	{
		write(1, map.map[map.y], str_len(map.map[map.y]));
		map.y++;
		write(1, "\n", 1);
	}
}
