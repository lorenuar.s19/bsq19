/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split_to_tab.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/19 15:23:47 by lorenuar          #+#    #+#             */
/*   Updated: 2019/11/20 12:27:01 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

int				charset(char c)
{
	if (c == '\n')
		return (1);
	return (0);
}

char			*str_ncpy(char *dest, char *src, unsigned int n)
{
	unsigned int i;

	i = 0;
	while (src[i] && i < n)
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

unsigned int	count_words(char *str)
{
	unsigned int i;
	unsigned int num_words;
	unsigned int word_l;

	num_words = 0;
	i = 0;
	while (str[i])
	{
		while (str[i] && charset(str[i]))
			i++;
		word_l = 0;
		while (str[i + word_l] && !charset(str[i + word_l]))
			word_l++;
		if (word_l)
			num_words++;
		i += word_l;
	}
	return (num_words);
}

char			**fill_tab(char **tab, char *str, unsigned int num_words)
{
	unsigned int word_l;
	unsigned int i;
	unsigned int j;

	i = 0;
	j = 0;
	while (i < num_words)
	{
		while (str[j] && charset(str[j]))
			j++;
		word_l = 0;
		while (str[j + word_l] && !charset(str[j + word_l]))
			word_l++;
		if ((tab[i] = malloc(sizeof(char) * (word_l + 1))) == 0)
			return (0);
		str_ncpy(tab[i], &str[j], word_l);
		j += word_l;
		i++;
	}
	return (tab);
}

char			**split_to_tab(char *str)
{
	char			**tab;
	unsigned int	num_words;

	num_words = count_words(str);
	if ((tab = malloc(sizeof(char*) * (num_words + 1))) == 0)
		return (0);
	tab = fill_tab(tab, str, num_words);
	tab[num_words] = 0;
	return (tab);
}
